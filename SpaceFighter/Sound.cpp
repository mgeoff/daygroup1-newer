#include "Sound.h"

/*  Constructors/Destructor
* * * * * * * * * * * * */
Sound::Sound()
{
    this->mAssetPath = "";
    this->mAssetName = "";
    this->mGain = 1.0f;
    this->mPan = 0.0f;
    this->mSpeed = 1.0f;
    this->mPlaymode = Once;

    this->pSample = NULL;
}

Sound::Sound(std::string assetPath, std::string assetName, float gain, float pan, float speed, Playmode playmode)
{
    this->mAssetPath = assetPath;
    this->mAssetName = assetName;
    this->mGain = gain;
    this->mPan = pan;
    this->mSpeed = speed;
    this->mPlaymode = playmode;

    this->pSample = al_load_sample((assetPath + assetName).c_str());
}

Sound::Sound(const Sound& other)
{
    this->mAssetPath = other.mAssetPath;
    this->mAssetName = other.mAssetName;
    this->mGain = other.mGain;
    this->mPan = other.mPan;
    this->mSpeed = other.mSpeed;
    this->mPlaymode = other.mPlaymode;

    this->pSample = al_load_sample((mAssetPath + mAssetName).c_str());
}

Sound::~Sound()
{
    al_destroy_sample(pSample);
}


void Sound::play()
{
    al_play_sample(
        pSample,
        mGain,
        mPan,
        mSpeed,
        getPlaymode(mPlaymode),
        NULL);
}