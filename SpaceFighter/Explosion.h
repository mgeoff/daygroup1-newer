#pragma once

#include "GameObject.h"
#include "Weapon.h"

class Explosion : public GameObject
{

public:

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void Update(const GameTime* pGameTime);

private:

	Texture* m_pTexture;

	static Level* s_pCurrentLevel;
};
