#include "Explosion.h"
#include "GameObject.h"
#include "Level.h"

Level* GameObject::s_pCurrentLevel = nullptr;

void Explosion::LoadContent(ResourceManager* pResourceManager)
{
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Explosion.png");
}

void Explosion::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter());
	}
}

void Explosion::Update(const GameTime* pGameTime)
{
	if (IsActive() && s_pCurrentLevel)
	{
		s_pCurrentLevel->UpdateSectorPosition(this);
	}
}